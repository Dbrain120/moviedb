package com.example.moviedatabaseapp.util

const val BASE_URL = "https://api.themoviedb.org/3/"
const val RESPONSE = "response"
const val MOVIE = "movie"
const val PATH = "movie/popular"
const val API_KEY = "e697aaa7d920e68de8de3beb170734ee"
const val LANGUAGE = "us-EN"
const val PAGE = 1